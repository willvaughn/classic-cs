from bisect import bisect_left
from typing import Callable, Iterable, Generic, List, Optional, Sequence, Set, TypeVar

T = TypeVar("T")


def linear_contains(xs: Iterable[T], target: T) -> bool:
    for x in xs:
        if x == target:
            return True
    return False


def binary_contains(xs: Sequence[T], target: T) -> bool:
    i = bisect_left(xs, target)
    if i != len(xs) and xs[i] == target:
        return True
    return False


class Stack(Generic[T]):
    def __init__(self):
        self._container: List[T] = []

    @property
    def is_empty(self) -> bool:
        return not self._container

    def push(self, item: T) -> None:
        self._container.append(item)

    def pop(self) -> T:
        return self._container.pop()

    def __repr__(self) -> str:
        return repr(self._container)


class Node(Generic[T]):
    def __init__(
        self,
        state: T,
        parent: Optional["Node"],
        cost: float = 0.0,
        heuristic: float = 0.0,
    ):
        self.state = state
        self.parent = parent
        self.cost = cost
        self.heuristic = heuristic

    def __lt__(self, other: "Node") -> bool:
        return (self.cost + self.heuristic) < (other.cost + other.heuristic)


def dfs(
    initial: T, goal_test: Callable[[T], bool], successors: Callable[[T], List[T]]
) -> Optional[Node]:
    frontier: Stack[Node[T]] = Stack()
    frontier.push(Node(initial, None))
    explored: Set[T] = {initial}

    while not frontier.is_empty:
        current_node = frontier.pop()
        current_state = current_node.state

        if goal_test(current_state):
            return current_node

        for child in successors(current_state):
            if child in explored:
                continue
            explored.add(child)
            frontier.push(Node(child, current_node))

    return None


def node_to_path(node: Node[T]) -> List[T]:
    path: List[T] = [node.state]
    while node.parent is not None:
        node = node.parent
        path.append(node.state)
    return list(reversed(path))
