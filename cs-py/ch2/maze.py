import random
from enum import Enum
from typing import List, NamedTuple, Optional
from ch2.generic_search import dfs, node_to_path


class Cell(str, Enum):
    EMPTY = " "
    BLOCKED = "X"
    START = "S"
    GOAL = "G"
    PATH = "*"


class MazeLocation(NamedTuple):
    row: int
    column: int

    def is_in_bounds(self, rows: int, columns: int) -> bool:
        return (0 <= self.row < rows) and (0 <= self.column < columns)


class Maze:
    def __init__(
        self,
        rows: int,
        columns: int,
        grid: List[List[Cell]],
        start: MazeLocation,
        goal: MazeLocation,
    ):
        self._rows = rows
        self._columns = columns
        self._grid = grid
        self._start = start
        self._goal = goal

    @property
    def start(self) -> MazeLocation:
        return self._start

    @property
    def goal(self) -> MazeLocation:
        return self._goal

    def __str__(self):
        out = ""
        for row in self._grid:
            out += "".join([c.value for c in row]) + "\n"
        return out

    def goal_test(self, ml: MazeLocation) -> bool:
        return ml == self.goal

    def successors(self, ml: MazeLocation) -> List[MazeLocation]:
        locations = []
        if (
            ml.row + 1 < self._rows
            and self._grid[ml.row + 1][ml.column] != Cell.BLOCKED
        ):
            locations.append(MazeLocation(ml.row + 1, ml.column))
        if ml.row - 1 >= 0 and self._grid[ml.row - 1][ml.column] != Cell.BLOCKED:
            locations.append(MazeLocation(ml.row - 1, ml.column))
        if (
            ml.column + 1 < self._columns
            and self._grid[ml.row][ml.column + 1] != Cell.BLOCKED
        ):
            locations.append(MazeLocation(ml.row, ml.column + 1))
        if ml.column - 1 >= 0 and self._grid[ml.row][ml.column - 1] != Cell.BLOCKED:
            locations.append(MazeLocation(ml.row, ml.column - 1))
        return locations

    def mark(self, path: List[MazeLocation]):
        for ml in path:
            self._grid[ml.row][ml.column] = Cell.PATH
        self._grid[self.start.row][self.start.column] = Cell.START
        self._grid[self.goal.row][self.goal.column] = Cell.GOAL

    def clear(self, path: List[MazeLocation]):
        for ml in path:
            self._grid[ml.row][ml.column] = Cell.EMPTY
        self._grid[self.start.row][self.start.column] = Cell.START
        self._grid[self.goal.row][self.goal.column] = Cell.GOAL


def random_cell_value(sparseness: float):
    if random.uniform(0, 1) < sparseness:
        return Cell.BLOCKED
    return Cell.EMPTY


def create_maze(
    rows: int = 10,
    columns: int = 10,
    sparseness: float = 0.2,
    start: MazeLocation = MazeLocation(0, 0),
    goal: Optional[MazeLocation] = None,
):
    if goal is None:
        goal = MazeLocation(rows - 1, columns - 1)
    for ml in (start, goal):
        if not ml.is_in_bounds(rows, columns):
            raise ValueError(
                f"Maze location {ml} is out of bounds in grid {rows}x{columns}"
            )
    grid = [
        [random_cell_value(sparseness) for _ in range(columns)] for _ in range(rows)
    ]
    grid[start.row][start.column] = Cell.START
    grid[goal.row][goal.column] = Cell.GOAL

    return Maze(rows, columns, grid, start, goal)


if __name__ == "__main__":
    m = create_maze()
    print(m)
    solution1 = dfs(m.start, m.goal_test, m.successors)
    if solution1 is None:
        print("No solution found using depth-first search.")
    else:
        path1 = node_to_path(solution1)
        m.mark(path1)
        print(m)
        m.clear(path1)
        print("Maze reset")
