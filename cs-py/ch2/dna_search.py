"""
DNA Search
"""
from enum import IntEnum
from typing import List, Tuple

from ch2.generic_search import linear_contains, binary_contains


class Nucleotide(IntEnum):
    A = 0
    C = 1
    G = 2
    T = 3


Codon = Tuple[Nucleotide, Nucleotide, Nucleotide]
Gene = List[Codon]


def string_to_gene(s: str) -> Gene:
    gene: Gene = []
    for i in range(0, len(s), 3):
        if (i + 2) >= len(s):
            return gene
        codon = (Nucleotide[s[i]], Nucleotide[s[i + 1]], Nucleotide[s[i + 2]])
        gene.append(codon)
    return gene


if __name__ == "__main__":
    gene_str = "ACGTGGCTCTCTAACGTACGTACGTACGGGGTTTATATATACCCTAGGACTCCCTTT"
    my_gene = string_to_gene(gene_str)
    acg = (Nucleotide.A, Nucleotide.C, Nucleotide.G)
    gat = (Nucleotide.G, Nucleotide.A, Nucleotide.T)

    linear_res1 = linear_contains(my_gene, acg)
    print(linear_res1)
    assert linear_res1
    binary_res1 = binary_contains(sorted(my_gene), acg)
    print(binary_res1)
    assert binary_res1

    linear_res2 = linear_contains(my_gene, gat)
    print(linear_res2)
    assert not linear_res2
    binary_res2 = binary_contains(sorted(my_gene), gat)
    print(binary_res2)
    assert not binary_res2
