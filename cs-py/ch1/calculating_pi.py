"""
Calculating Pi

Leibniz formula for pi.
"""

def calculate_pi(n_terms: int) -> float:
    denominator: float = 1.0
    operation: float = 1.0
    pi: float = 0.0
    for _ in range(n_terms):
        pi += operation * (4.0 / denominator)
        denominator += 2.0
        operation *= -1.0
    return pi


if __name__ == "__main__":
    print(calculate_pi(1000000))
