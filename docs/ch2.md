# Chapter 2: Search

## Binary search

### Python

[generic_search.py](../cs-py/ch2/generic_search.py)
[dna_search.py](../cs-py/ch2/dna_search.py)

I did python first here. I did a little extra and made the `generic_search` code a package. I also used the standard lib `bisect` module and `bisect_left` in particular to implement the binary search. I didn't seem to need all the extra `Protocol` stuff the book uses in order to pass mypy.


### Clojure

[dna_search.clj](../cs-clj/src/cs_clj/ch2/dna_search.clj)

I didn't genericize this as much in Clojure as I did in python. I made `linear-contains?` and `binary-contains?` and kept them inside the `dna-search` namespace.

The linear search is really just a wrapper around [`some`](https://clojuredocs.org/clojure.core/some).

The binary search uses recursiion rather than a loop as in the book; seemed like the way to go in clojure. It was key for me to find the [`compare`](https://clojuredocs.org/clojure.core/compare) function in order to compare the codon values. The codon values are vectors of keywords `:a :c :g :t`. It seems keywords are the replacement for Enums in clojure, though it did mean having to do some extra validation of valid characters. My data model for genes is a simple nested vector `[[:a :c :g] [:g :a :t] ...]`. It was the simplest thing I could think of, though I will say that it took me a little while to discover how to convert my string of characters into these keywords. The chars need to be converted to `str`, the `str` needs to be coverted to a `kw`, and the original incoming string needed to be grouped into triples using  [`partition`](https://clojuredocs.org/clojure.core/partition).

## Depth-Firt search

### Clojure

This was a challenge. I feel like this has become my first real Clojure program.

#### A lot of effort to be lazy

I spent a chunk of time trying to figure out a way to make the `dfs` function produce a stream of /all/ solutions to the maze, but this was really hard to get right. I think I don't have quite enough experience sequence processing and using the clojure immutable structures to really pull this off. I ended up using a `loop` to do this in clojure very similar to how it is done in python. In the python example, they just look at a single `Node` and give each a reference to it's parent. Then at the end, they reconstruct the path. Without knowing how to replicate the `Node` class in clojure, and specifically its by reference `parent` attribute, I decided accumulate the path to every node in the loop, passing it along in every iteration. When the goal is found the path is the result. 

#### Soliciting code review

I made a reddit post [here](https://www.reddit.com/r/Clojure/comments/ezcnt9/would_you_could_kindly_code_review_a_depthfirst/) and a stackexchange post [here](https://codereview.stackexchange.com/questions/236685/clojure-depth-first-search-maze-problem-from-classic-cs-problems-in-python-book). I received excellent feedback from kind strangers that both helped me write more idiomatic readable clojure, and structurally changed the way I was representing the maze as data. It was such a good experience, I learned so much more from these folks than I could have on my own.

#### Destructuring is crucial

It's a little confusing to read honestly, but I felt like I could barely do anything without leaning so heavily on destructuring. In Python, we're always reaching into objects to get values with `__getattr__` (`foo.bar`) or `__get__` (`foo["bar"]`) syntax. In Clojure, the data is still getting passed around as a group but destructuring is often the way to grab it, e.g. `(let [[_ _ grid] maze])`. This came up in arguments to many functions as well. Creating default values for keyword arguments to the `create-maze` function was a learning experience in clojure which also heavily required me to read docs on destructuring syntax and techniques.

## Breadth-First Search

Very simple adaptation of the depth-first algorithm to use a different structure for the `frontier`. I found `clojure.lang.PersistentQueue/EMPTY` and used that as a FIFO Queue. It took me a little while to find this thing and to grok the examples I was reading online about it. It's not extremely well documented and you have to use Java Interop from clojure to use it, but it ended up working very well.

## A* Search

[astar](../cs-clj/src/cs_clj/ch2/generic_search.clj)
The A* required me to alter the algorithm to pass a few more things around in the loop, cost and heuristic values. It wasn't extremely difficult
