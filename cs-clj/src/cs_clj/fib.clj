(ns cs-clj.fib)

;; This is my own version of fibonacci done with an interative approach.
(defn nth-fib
  "Returns the nth fibonacci value"
  [n]
  (if (< n 2)
    n
    (loop [i 1
           lst 0
           nxt 1]
      (if (>= i n)
        nxt
        (recur (inc i) nxt (+' lst nxt))))))

;; This one I found on the internet and it took me awhile to understand.
;; It generates a lazy sequence. Using lazy-cat it seeds the sequence with the
;; first two members of the fibonacci sequence 0 and 1. The rest of the sequence
;; is formed recursively by adding together values of two versions of the fibonacci
;; sequence which are offset by one value. This is what codifies the meaning of the
;; fibonacci sequence. The values of (rest fib-seq) are one value ahead. Summing
;; these values with the values of fib-seq while generating a new sequence is
;; lazily creating the fibonacci sequence.
;; This is mind bending, concise, but ultimately pretty confusing.
(def fib-seq
  "Lazy sequence of fibonacci numbers"
  (lazy-cat [0, 1] (map +' fib-seq (rest fib-seq))))
  
