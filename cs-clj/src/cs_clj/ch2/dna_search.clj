(ns cs-clj.ch2.dna-search)

(defn linear-contains? [gene key-codon]
  (some #(when (= % key-codon) %) gene))

(defn binary-contains? [gene key-codon]
  (defn search-region [low high]
    (when (<= low high)
      (let [mid (quot (+ low high) 2)
            codon (nth gene mid)
            res (compare codon key-codon)]
        (cond (neg? res) (recur (inc mid) high)
              (pos? res) (recur low (dec mid))
              :else codon))))
  (search-region 0 (dec (count gene))))

(def nucleotides {:a 0 :c 1 :g 2 :t 3})

(defn make-nucleotide [char]
  (let [kw (keyword (str char))]
    (if (contains? nucleotides kw)
      kw
      (throw (Exception. (format "Unknown nucleotide character: \"%s\"" char))))))

(defn make-codon [chars-triple]
  (apply vector (map make-nucleotide chars-triple)))

(defn string-to-gene [s]
  (apply vector (map make-codon (partition 3 s))))

;; lein run -m cs-clj.ch2.dna-search
(defn -main []
  (let [gene (string-to-gene "acgcattaggatacgcaaaacga")
        sorted-gene (sort gene)]
    (do
      (println (str "Using gene " gene))
      (println "Searching for \"cat\" codon.")
      (println (binary-contains? sorted-gene [:c :a :t]))
      (println "Searching for \"att\"")
      (println (binary-contains? sorted-gene [:a :t :t])))))
