(ns cs-clj.ch2.generic-search
  (:require [clojure.data.priority-map :refer [priority-map-keyfn-by]]))

(defn gen-search
  [start goal? successors make-frontier]
  (loop [frontier (make-frontier [start, []])
         explored #{start}]
    (when-let [[loc path] (peek frontier)]
      (let [next-path (conj path loc)]
        (if (goal? loc)
          next-path
          (recur (->> (successors loc)
                      (remove explored)
                      (map (fn [l] [l next-path]))
                      (apply conj (pop frontier)))
                 (apply conj explored (successors loc))))))))

(defn queue
  ([] (clojure.lang.PersistentQueue/EMPTY))
  ([& coll]
   (reduce conj clojure.lang.PersistentQueue/EMPTY coll)))

(defn dfs
  [start goal? successors]
  (gen-search start goal? successors list))

(defn bfs
  [start goal? successors]
  (gen-search start goal? successors queue))

(defn total-cost
  [[cost heur _]]
  (+ cost heur))

(defn astar
  [start goal? successors heuristic]
  (loop [frontier (into () (priority-map-keyfn-by total-cost < start [0.0 (heuristic start) []]))
         explored {start 0.0}]
    (when-let [[cur-loc [cost heur path]] (peek frontier)]
      (let [next-path (conj path cur-loc)
            next-cost (+ cost 1)
            already-explored? (fn [l]
                                (and (contains? explored l)
                                     (<= (get explored l) next-cost)))
            make-frontier-entry (fn [l]
                                  [l [next-cost (heuristic l) next-path]])]
        (if (goal? cur-loc)
          next-path
          (recur (->> (successors cur-loc)
                      (remove already-explored?)
                      (map make-frontier-entry)
                      (apply conj (pop frontier))
                      (into ()))
                 (merge explored (zipmap (successors cur-loc) (repeat next-cost))))))))) 
