(ns cs-clj.ch2.maze
  (:require [clojure.string :as string]
            [cs-clj.ch2.generic-search :refer [astar bfs]]))

(def cell-display
  {:empty " "
   :blocked "X"
   :start "S"
   :goal "G"
   :path "*"})

(defn random-cell
  [sparsity]
  (if (< (rand) sparsity)
    :blocked
    :empty))

(defn empty-grid [[w h]]
  {:size [w h] :cells (vec (repeat (* w h) :empty))})

(defn grid-index [{[w _] :size} [x y]]
  (+ x (* w y)))

(defn in-bounds? [{[w h] :size} [x y]]
  (and (> x 0) (>= y 0) (< x w) (< y h)))

(defn blocked?
  "Is the location at loc occupied in the maze by a :blocked character?"
  [{cells :cells :as grid} loc]
  (= (get cells (grid-index grid loc)) :blocked))

(def open? (complement blocked?))

(defn generate-grid [grid gen-cell]
  (update grid :cells (partial mapv (fn [_] (gen-cell)))))

(defn mark-grid-with
  [grid loc-vals]
  (let [idx-vals (map (fn [[loc v]]
                        [(grid-index grid loc) v])
                      loc-vals)]
    ;; TODO: Can I do this with update?
    (assoc grid :cells (reduce (fn [out-cells [i v]]
                                 (assoc out-cells i v))
                               (:cells grid)
                               idx-vals))))

;; TODO: think about whether maze "objects" could be a record.
;; TODO: Is there a good Protocol opportunity here with mark-path, blocked? display-maze, successors?
(defn create-maze
  "Creates randomly generated maze map."
  [& {:keys [rows cols sparsity start provided-goal]
      :or {rows 10
           cols 10
           sparsity 0.2
           start [0 0]
           provided-goal nil}}]
  (let [goal (if (nil? provided-goal)
               [(dec rows) (dec cols)]
               provided-goal)
        rand-grid (generate-grid (empty-grid [rows cols]) #(random-cell sparsity))
        grid (mark-grid-with rand-grid [[start :start]
                                        [goal :goal]])]
    {:start start
     :goal goal
     :grid grid}))

(defn display-maze
  "Formats the maze grid to a legible string value."
  [{{[w _] :size cells :cells} :grid}]
  (->> cells
       (map cell-display)
       (partition w)
       (map string/join)
       (string/join "\n")))

(defn successors
  "Gets sequence of valid moves from the given location in the maze."
  [maze loc]
  (let [{grid :grid} maze
        [x y] loc
        candidates [[(inc x) y]
                    [(dec x) y]
                    [x (inc y)]
                    [x (dec y)]]]
    (->> candidates
         (filter (partial in-bounds? grid))
         (filter (partial open? grid)))))

(defn mark-path
  "Mark the path through the maze with * characters. Returns altered maze."
  [maze path]
  (let [path-locs (subvec path 1 (dec (count path)))
        loc-vals (map (fn [path-loc]
                        [path-loc :path])
                      path-locs)]
    ;; TODO: Can I be using update here?
    (assoc maze
           :grid (mark-grid-with (:grid maze) loc-vals))))

(defn manhattan-distance
  [[x1 y1] [x2, y2]]
  (let [x-dist (Math/abs (- x2 x1))
        y-dist (Math/abs (- y2 y1))]
    (+ x-dist y-dist)))

;; lein run -m cs-clj.ch2.maze
(defn -main []
  (let [maze (create-maze)
        goal (:goal maze)
        start (:start maze)
        goal? (partial = goal)
        distance-fn (partial manhattan-distance goal)
        successors-fn (partial successors maze)
        bfs-path (bfs start goal? successors-fn)
        astar-path (astar start goal? successors-fn distance-fn)]
    (println (display-maze maze))
    (if (nil? bfs-path)
      (println "Could not solve bfs")
      (println (display-maze (mark-path maze bfs-path))))
    (if (nil? astar-path)
      (println "Could not solve astar")
      (println (display-maze (mark-path maze astar-path))))))
