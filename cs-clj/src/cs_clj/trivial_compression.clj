(ns cs-clj.trivial-compression)

(defn decompress-gene [compressed-gene]
  "Converts compressed integer form of a gene, back to a human readable decompressed string."
  (let [bit-length (- (count (Integer/toBinaryString compressed-gene)) 1)] 
    (apply str (reverse (map (fn [i]
                      (let [bits (bit-and (bit-shift-right compressed-gene i) 2r11)]
                        (cond (= bits 2r00) \A
                              (= bits 2r01) \C
                              (= bits 2r10) \G
                              (= bits 2r11) \T)))
                    (range 0 bit-length 2))))))

(defn compress-gene [gene-string]
  "Compresses a string representation of a gene to a space efficient integer."
  (reduce (fn [bits, s]
            (let [shifted (bit-shift-left bits 2)
                  gene-mask (cond (= s \A) 2r00
                                  (= s \C) 2r01
                                  (= s \G) 2r10
                                  (= s \T) 2r11)]
              (bit-or shifted gene-mask)))
              1
              gene-string))


;; original: str = "TAGGGATTAACCGTTATATATATATAGCCATGGATCGATTATATAGGGATTAACCGTTATATATATATAGCCATGGATCGATTATA" * 100
;; print(f"original is {getsizeof(original)} bytes")
;; compressed = CompressedGene(original)
;; print(f"compressed is {getsizeof(compressed.bit_string)} bytes")
;; print(compressed.bit_string)
;; print(compressed)
;; print(f"original and decompressed are the same: {original == str(compressed)}")
