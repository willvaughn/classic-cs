(ns cs-clj.hanoi)


(defn hanoi [n from to via]
  (when (pos? n)
    (lazy-cat (hanoi (dec n) from via to)
              (cons [from '-> to]
                    (hanoi (dec n) via to from)))))

(defn -main
  [& args]
  (let [num-discs 3
        tower-a (apply list (range num-discs))
        tower-b ()
        tower-c ()]
    (hanoi num-discs tower-a tower-b tower-c)))
